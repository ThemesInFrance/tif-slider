<?php
/**
 * Plugin Name: Tif - Slider
 * Plugin URI: https://themesinfrance.fr/plugins/home-slider/
 * Description: Create a beautiful slider in minutes on your homepage.
 * Version: 0.1
 * Author: Frédéric Caffin
 * Author URI: https://themesinfrance.fr
 * Text Domain: tif-slider
 * Domain Path: /inc/lang
 *
 * License: GPLv2 or later

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * ( at your option ) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.46
 */

/**
 * -slider ( => tif-slider )
 * _slider ( => tif_slider )
 * _Slider ( => Tif_Slider )
 * _SLIDER ( => TIF_SLIDER )
 * Slider
 */

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'TIF_SLIDER_VERSION', '1.0' );
define( 'TIF_SLIDER_DIR', plugin_dir_path( __FILE__ ) );
define( 'TIF_SLIDER_URL', plugins_url( '/', __FILE__ ) );
define( 'TIF_SLIDER_IMG_URL', TIF_SLIDER_URL . 'assets/img/' );
define( 'TIF_SLIDER_ADMIN_IMAGES_URL', plugins_url( '/assets/img/admin', __FILE__ ) ) ;

/**
 * register_uninstall_hook( $file, $callback );
 *
 * @link https://codex.wordpress.org/Function_Reference/register_uninstall_hook
 */
register_uninstall_hook( __FILE__, 'tif_slider_uninstall' );
function tif_slider_uninstall() {

	delete_option( 'tif_plugin_slider' );

}

if ( ! defined( 'TIF_COMPONENTS_DIR' ) )
	define( 'TIF_COMPONENTS_DIR', ( get_theme_mod( 'tif_components_version' ) ? get_template_directory() . '/tif/' : plugin_dir_path( __FILE__ ) . 'tif/' ) );

require_once TIF_COMPONENTS_DIR . 'init.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-minify.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-assets.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-posts-lists.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-thumbnail.php';
require_once TIF_COMPONENTS_DIR . 'components/tif-mail.php';


// Slider Setup data
require_once 'inc/tif-setup-data.php';

// Slider Customizer
require_once 'inc/tif-customizer.php';

// Slider
require_once 'inc/tif-functions.php';

if( is_admin() ) {

	// Back
	require_once 'inc/admin/tif-settings-form.php';
	require_once 'inc/admin/tif-register.php';

}

/**
 * Slider Class Init
 */
class Tif_Slider_Init {

	/**
	 * Setup class.
	**/
	function __construct() {

		// Slider Back scripts
		// add_action( 'admin_enqueue_scripts', array( $this, 'tif_slider_admin_scripts' ) );

		// Get Slider add_action
		add_action( 'after_setup_theme', array( $this, 'tif_get_slider_callback' ) );

		// Slider Front scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'tif_slider_public_scripts' ), 110 );

		// Slider Translation
		add_action( 'init', array( $this, 'tif_slider_translation' ) );
		// add_action( 'admin_init', array( $this, 'tif_slider_polylang' ) );

		// ... Slider Images Size ...........................................
		add_action( 'after_setup_theme', array( $this, 'tif_slider_add_image_size' ) );
		add_filter( 'intermediate_image_sizes_advanced', array( $this, 'tif_slider_remove_image_size' ), 10, 3 );

		// ... Slider Assets Generation .....................................
		// Add Slider CSS to generated main CSS if configured
		add_filter( 'tif_main_css', array( $this, 'tif_get_compiled_slider_css' ), 30 );

		// Add Slider JS to generated main JS if configured
		add_filter( 'tif_main_js', array( $this, 'tif_get_slider_js' ), 30 );

		// Build standalone Slider CSS
		add_action( 'after_setup_theme', array( $this, 'tif_generate_css_from_fo' ), 20 );
		add_action( 'customize_save_after', array( $this, 'tif_create_slider_css' ), 20, false );

		// Inline Slider CSS if required
		add_action( 'wp_head', array( $this, 'tif_slider_inline_css' ), 20 );

		if( is_admin() ) {

			// Display admin notices
			add_action( 'admin_notices', array( $this, 'tif_slider_admin_notice' ), 10 );

			// Slider Menu
			add_action( 'admin_menu', array( $this, 'tif_slider_menu' ), 100 );

			// Slider Capability
			add_filter( 'option_page_capability_tif-slider', array( $this, 'tif_slider_option_page_capability' ) );

			if ( ! is_customize_preview() )
				// Slider Register
				add_action( 'admin_init', array( $this, 'tif_slider_register' ) );

		}

	}

	/**
	 * Display admin notices
	 */
	public function tif_slider_admin_notice() {

		$settings_errors = get_settings_errors( 'slider_settings_error', false );

		// DEBUG:
		// tif_print_r($settings_errors);

		// Get the current screen
		$screen = get_current_screen();

		// Return if not plugin settings page
		if ( $screen->id !== 'ajustements_page_tif-slider-options') return;

		// Checks if settings updated
		if ( isset( $settings_errors[0]['type'] ) && $settings_errors[0]['type'] == 'updated' ) {

			$this->tif_generate_css_from_bo();
			echo '<div class="notice notice-success"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		} elseif ( isset ( $settings_errors[0]['type'] )) {

			echo '<div class="notice notice-warning"><p>' . esc_html__( $settings_errors[0]['message'] ) . '</p></div>';

		}

	}

	/**
	 * Get Slider add_action
	 */
	static function tif_get_slider_callback() {

		add_action( 'homepage', 'tif_render_slider', 10 );

	}

	/**
	 * Get Slider generated assets path
	 */
	static function tif_plugin_assets_dir() {

		$tif_dir = array();
		$dirs = wp_upload_dir();
		$tif_dir['basedir'] = $dirs['basedir'] . '/plugins/tif-slider';
		$tif_dir['baseurl'] = $dirs['baseurl'] . '/plugins/tif-slider';

		/**
		 * @see https://developer.wordpress.org/reference/functions/wp_mkdir_p/
		 */

		$mkdir = array(
			'',
			'/assets',
			'/assets/css',
			// '/assets/css/blocks',
			'/assets/js',
			'/assets/img',
			// '/assets/fonts'
		);

		foreach ( $mkdir as $key ) {

			if ( ! is_dir( $tif_dir['basedir'] . $key ) )
				wp_mkdir_p( $tif_dir['basedir'] . $key );

		}

		return $tif_dir;

	}

	/**
	 * Slider admin scripts
	 */
	public function tif_slider_admin_scripts( $hook ) {

		if ( 'settings_page_tif-slider-options' != $hook )
			return;

		// Slider Back CSS
		wp_register_style( 'tif-slider-admin', TIF_SLIDER_URL . 'assets/css/admin/style' . tif_get_min_suffix() . '.css' );
		wp_enqueue_style( 'tif-slider-admin' );

		// Slider Back JS
		wp_register_script( 'tif-slider-admin', TIF_SLIDER_URL . 'assets/js/admin/script' . tif_get_min_suffix() . '.js', $deps, time(), true );
		wp_enqueue_script( 'tif-slider-admin' );

	}

	/**
	 * Slider front scripts
	 */
	public function tif_slider_public_scripts() {

		if ( ! is_home() && ! is_front_page() && ! is_page_template( 'templates/template-homepage.php' ) )
			return;

		$tif_generated = tif_get_option( 'plugin_slider', 'tif_init,generated', 'multicheck' );
		$tif_options   = tif_get_option( 'plugin_slider', 'tif_options', 'array' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		$deps          = get_theme_mod( 'tif_components_version' ) ? array( 'tif-main' ) : false;

		if ( ! $deps || ( $deps && ! in_array( 'css', $tif_generated ) && ! is_customize_preview() ) ) {

			// Get Slider CSS dir
			$tif_dir     = $this->tif_plugin_assets_dir();
			$filetocheck = $tif_dir['basedir'] . '/assets/css/style.css';
			$version     = file_exists( $filetocheck ) ? date ( 'YdmHis', filemtime( $filetocheck ) ) : time();

			// Slider Front CSS if not added to TIF Main CSS
			wp_register_style( 'tif-slider', $tif_dir['baseurl'] . '/assets/css/style' . tif_get_min_suffix() . '.css', $deps, $version );
			wp_enqueue_style( 'tif-slider' );

		}

		if ( (int)$tif_options['how_many'] >=2 && (int)$tif_options['delay'] > 0 ) {

			$localized = 'tif-scripts';

			if ( ! $deps || ( $deps && ! in_array( 'js', $tif_generated ) ) ) {

				$localized	= 'tif-slider' ;

				// Slider Front JS if not added to TIF Main JS
				wp_register_script( 'tif-slider', TIF_SLIDER_URL . 'assets/js/script' . tif_get_min_suffix() . '.js', $deps, time(), true );
				wp_enqueue_script( 'tif-slider' );

			}

			wp_localize_script(
				$localized,
				'slider_options', array(
					'how_many' => (int)$tif_options['how_many'],
					'delay' => (int)$tif_options['delay'],
				)
			);

		}

	}

	/**
	 * Slider capability
	 */
	public function tif_slider_option_page_capability( $capability ) {

		if ( current_user_can( 'edit_posts' ) )
			return 'edit_posts';

	}

	/**
	 * Slider admin menu
	 */
	public function tif_slider_menu() {

		// @link https://www.base64-image.de/
		// @link http://b64.io/

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ) ) ;

		$unify_menus = get_option( 'tif_plugin_tweaks');
		$unify_menus = isset( $unify_menus['tif_init']['unify_menus'] ) && $unify_menus['tif_init']['unify_menus'] ? true : false ;

		if ( ! empty ( $GLOBALS['admin_page_hooks']['tif-tweaks-options'] ) && $unify_menus ) {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_submenu_page/
			 */
			$hook = add_submenu_page(
				'tif-tweaks-options', 						// $parent_slug
				esc_html__( 'Slider', 'tif-slider' ), 		// $page_title
				esc_html__( 'Slider', 'tif-slider' ), 		// $menu_title
				$capability, 								// $capability
				'tif-slider-options',						// $menu_slug
				'tif_slider_options_page',					// $function
				1											// $position
			);

		} else {

			/**
			 * @link https://developer.wordpress.org/reference/functions/add_options_page/
			 */
			$hook = add_options_page(
				esc_html__( 'Tif - Slider', 'tif-slider' ),	// $page_title
				esc_html__( 'Tif - Slider', 'tif-slider' ),	// $menu_title
				$capability,								// $capability
				'tif-slider-options',						// $menu_slug
				'tif_slider_options_page'					// $function
			);

		}

	}

	/**
	 * register_setting( $option_group, $option_name, $sanitize_callback );
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_setting
	 */
	public function tif_slider_register() {

		if( ! is_customize_preview() )
			register_setting( 'tif-slider', 'tif_plugin_slider', 'tif_slider_sanitize' );

	}

	/**
	 * Loads Slider translated strings
	 *
	 * @link https://developer.wordpress.org/reference/functions/load_plugin_textdomain/
	 */
	public function tif_slider_translation() {

		load_plugin_textdomain( 'tif-slider', false, basename( dirname( __FILE__ ) ) . '/inc/lang' );

	}

	/**
	 * Enable Slider translation with Polylang
	 *
	 * @link https://polylang.pro/doc/function-reference/
	 */
	public function tif_slider_polylang() {

		if( ! function_exists( 'pll_register_string' ) )
			return;

		$enabled = tif_get_option( 'plugin_slider', 'tif_options,how_many', 'absint' );

		for ( $i=1; $i <= $enabled; $i++ ) {

			pll_register_string(
				'#' . $i . ' : ' . esc_html__( 'Title', 'tif-slider' ),
				tif_get_option( 'plugin_slider', 'tif_slider' . $i . ',title', 'text' ),
				esc_html__( 'Tif - Slider', 'tif-slider' )
			);

			pll_register_string(
				'#' . $i . ' : ' . esc_html__( 'Text', 'tif-slider' ),
				tif_get_option( 'plugin_slider', 'tif_slider' . $i . ',text', 'text' ),
				esc_html__( 'Tif - Slider', 'tif-slider' )
			);

			pll_register_string(
				'#' . $i . ' : ' . esc_html__( 'Button text', 'tif-slider' ),
				tif_get_option( 'plugin_slider', 'tif_slider' . $i . ',btn1_txt', 'text' ),
				esc_html__( 'Tif - Slider', 'tif-slider' )
			);

			pll_register_string(
				'##' . $i . ' : ' . esc_html__( 'Button text', 'tif-slider' ),
				tif_get_option( 'plugin_slider', 'tif_slider' . $i . ',btn2_txt', 'text' ),
				esc_html__( 'Tif - Slider', 'tif-slider' )
			);

		}

	}

	// ... Slider Images Size ...............................................
	public function tif_slider_add_image_size() {

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		$tif_layout      = tif_get_option( 'plugin_slider', 'tif_layout', 'array' );

		$tif_thumb_ratio = (string)$tif_layout['thumb_ratio'];
		$tif_thumb_width = absint( $tif_layout['thumb_width'] );
		$sliderinfo      = $this->tif_image_info( $tif_thumb_ratio );
		add_image_size( 'tif-slider', $tif_thumb_width, round( $tif_thumb_width * $sliderinfo['ratio'] ), $sliderinfo['crop'] );

	}

	public function tif_slider_remove_image_size( $sizes ) {

		// Remove tif slide resolution for on-the-fly generation
		unset( $sizes[ 'tif-slider' ]);

		return $sizes;
	}

	// le filtre qui permet d'ajouter la nouvelle taille au gestionnaire de médias
	public function tif_image_info( $ratio ) {
		if ( $ratio == "uncropped" ) {
			$info['ratio'] = 1;
			$info['crop'] = false;
		} else {
			$ratio = ! is_array( $ratio ) ? explode( ',', $ratio ) : $ratio;
			$info['ratio'] = ($ratio[1] / $ratio[0]);
			$info['crop'] = true;
		}
		return $info;
	}

	// ... Slider Assets Generation .........................................

	/**
	 * Get Slider assets added to tif main generated
	 */
	public function tif_get_generated_assets() {

		$tif_generated = tif_get_option( 'plugin_slider', 'tif_init,generated', 'multicheck' );
		$tif_generated = is_array( $tif_generated ) ? $tif_generated : array();
		return $tif_generated;

	}

	/**
	 * Slider inline CSS for customize preview
	 */
	public function tif_slider_inline_css() {

		if ( ! is_customize_preview() )
			return;

		echo '<style type="text/css">';

		$this->tif_compile_slider_css( true );

		echo '</style>';

	}

	/**
	 * Get Slider compiled CSS
	 */
	public function tif_get_compiled_slider_css( $css ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(  ! class_exists ( 'Themes_In_France' )
			|| ! in_array( 'css', $tif_generated ) )
			return $css;

		$css .= $this->tif_compile_slider_css();

		return $css;

	}

	/**
	 * Add Slider CSS to tif-main.css if configured
	 */
	private function tif_compile_slider_css( $echo = false, $custom_css = false ) {

		$tif_css_enabled = tif_get_option( 'plugin_slider', 'tif_init,css_enabled', 'key' );
		$layout          = tif_get_option( 'plugin_slider', 'tif_layout', 'array' );
		$options         = tif_get_option( 'plugin_slider', 'tif_options', 'array' );
		$colors          = tif_get_option( 'plugin_slider', 'tif_colors', 'array' );

		$boxed_width = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : $layout['boxed_width'];

		$plugin_scss_components = array(
			'_cover',
			'_overlay',
		);

		if( class_exists( 'Themes_In_France' ) ) {

			$theme_scss_components = tif_get_theme_scss_components_enabled();

			foreach ($plugin_scss_components as $key => $value) {

				if( in_array( $value, $theme_scss_components) ) {
					unset($plugin_scss_components[$key]);
				}

			}

			$tif_breakpoints = tif_get_option( 'theme_assets', 'tif_css_breakpoints', 'multicheck' );
			$variables = array(
				'$breakpoints'    => '(
					sm: ' . (int)$tif_breakpoints[0] . 'px,
					md: ' . (int)$tif_breakpoints[1] . 'px,
					lg: ' . (int)$tif_breakpoints[2] . 'px,
					xl: ' . (int)$tif_breakpoints[3] . 'px,
				)',
			);

		} else {

			$variables = array(
				'$breakpoints'    => '(
					sm: 576px,
					md: 992px,
					lg: 1330px,
				)',
				'$utils-components'                     => '(
					_utils-global,
					_utils-spacers,
					_grillade
				)',
				'$tif-compiled-grid'                    => true,
				'$tif-compiled-grid-col'                => true,
				'$tif-compiled-grid-gap'                => true,
				'$tif-compiled-grid-col-span'           => true,
				'$tif-compiled-grid-has-bp'             => true,
				'$tif-compiled-grid-has-bp'             => true,
				'$grid-columns'                         => 3,
				'$grid-breakpoints'                     => '(
					sm: 576px,
					lg: 1330px,
				)',
				'$utils'                                => '(
					(hidden, display, none),
					(flex, display, flex),
				)',
			);

		}

		$variables = array_merge(
			$variables,
			array(
				'$scss-components'                      => '(' . implode( ',', $plugin_scss_components ) . ')',

				'$tif-width-primary'                    => tif_get_length_value( tif_sanitize_length ( $boxed_width ) ),
				'$tif-width-wide'                       => '1400px',

				'$tif-slider-alignment-align-items'     => (string)$layout['alignment']['align_items'],
				'$tif-slider-alignment-justify-content' => (string)$layout['alignment']['justify_content'],
				'$tif-slider-alignment-gap'             => tif_get_length_value( $layout['alignment']['gap'] ),
				'$tif-slider-how-many'                  => (int)$options['how_many'],
				'$tif-slider-hide-thumbnail-breakpoint' => null != $layout['thumb_hide'] ? '$' . (string)$layout['thumb_hide'] : false,
			)
		);

		$text_bg = tif_sanitize_multicheck( $colors['excerpt_bgcolor'] );
		if ( $text_bg['2'] > 0 )
			$variables = array_merge( $variables, array( '$tif-slider-has-text-bg' => true ) );

		$title_bg = tif_sanitize_multicheck( $colors['title_bgcolor'] );
		if ( $title_bg['2'] > 0 )
			$variables = array_merge( $variables, array( '$tif-slider-has-title-bg' => true ) );

		$css = null;

		// Compile Plugin CSS
		if ( $tif_css_enabled != 'custom' )
			$css .= tif_compile_scss(
				array(
					'origin'        => 'plugin',
					'components'    => array( '_style' ),
					'variables'     => $variables,
					'path'          => TIF_SLIDER_DIR . 'assets/scss'
				)
			);

		$custom_colors = new Tif_Custom_Colors;
		// $color = $custom_colors->tif_colors();

		$css .= '
		/* Tif Slider Colors */
		' .
		$custom_colors->tif_colors_custom(
			'.tif-slider .arrows',
			array(
				'color'        => '#ffffff',
			)
		) .

		$custom_colors->tif_colors_custom(
			array(
				'.tif-slider input[type="radio"]:checked + label',
				'.tif-slider.js .slider-pager button'
			),
			array(
				'bgcolor'      => '#ffffff',
				'color'        => true,
			)
		) .

		$custom_colors->tif_colors_custom(
			array(
				'.tif-slider.js .slider-pager button.current'
			),
			array(
				'bgcolor'      => '#999999',
				'color'        => true,
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-slider',
			array(
				'bgcolor'      => tif_sanitize_multicheck( $colors['bgcolor'] ),
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-slider .tif-overlay::after',
			array(
				'bgcolor'      => tif_sanitize_multicheck( $colors['overlay_bgcolor'] ),
				'important'    => true,
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-slider .slider-title span',
			array(
				'bgcolor'      => tif_sanitize_multicheck( $colors['title_bgcolor'] ),
				'color'        => tif_sanitize_multicheck( $colors['title_color'] ),
				'link'         => tif_sanitize_multicheck( $colors['title_color'] ),
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-slider .slider-sub-title span',
			array(
				'bgcolor'    => tif_sanitize_multicheck( $colors['subtitle_bgcolor'] ),
				'color'      => tif_sanitize_multicheck( $colors['subtitle_color'] ),
				'link'       => tif_sanitize_multicheck( $colors['subtitle_color'] ),
			)
		) .

		$custom_colors->tif_colors_custom(
			'.tif-slider .entry-content',
			array(
				'bgcolor'    => tif_sanitize_multicheck( $colors['excerpt_bgcolor'] ),
				'color'      => tif_sanitize_multicheck( $colors['excerpt_color'] ),
				'link'       => tif_sanitize_multicheck( $colors['excerpt_color'] ),
			)
		) .

		'';

		if ( $custom_css ) {
			// Add custom css after register backoffice form
			$custom_css = is_bool( $custom_css ) ? null : $custom_css ;
			$css .= strip_tags( $custom_css ) . "\n";

		} else {
			// Add custom css customize_save_after
			$css .= strip_tags( tif_get_option( 'plugin_slider', 'tif_init,custom_css', 'textarea' ) ) . "\n";
		}

		if ( $echo )
			echo $css;

		else
			return $css;

	}

	/**
	 * Create Plugin CSS
	 */
	public function tif_create_slider_css( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-slider' ) );

		// Get plugin CSS dir
		$tif_dir	= $this->tif_plugin_assets_dir();

		// Compile plugin CSS
		$plugin_css = $this->tif_compile_slider_css( false, $custom_css );

		// Create CSS
		tif_create_assets(
			array (
				'content'    => $plugin_css,
				'type'       => 'css',
				'path'       => $tif_dir['basedir'] . '/assets/css/',
				'name'       => 'style'
			)
		);

	}

	/**
	 * Generate Plugin css after register_setting()
	 */
	private function tif_generate_css_from_bo() {

		if ( ! tif_is_writable() )
			return;

		$capability = tif_get_submenu_capability( tif_get_option( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ) ) ;

		if ( ! current_user_can( $capability ) )
			wp_die( esc_html__( 'Unauthorized user.', 'tif-slider' ) );

		// Generate theme tif-main.css
		if( get_theme_mod( 'tif_components_version' ) ) {
			tif_create_theme_main_css( true );
			tif_create_theme_main_js( true );
		}

		// Generate plugin CSS
		$custom_css = null != get_option( 'tif_plugin_slider' )['tif_init']['custom_css'] ?
					get_option( 'tif_plugin_slider' )['tif_init']['custom_css'] :
					true;
		$generated  = new Tif_Slider_Init;
		$generated->tif_create_slider_css( strip_tags( $custom_css ) );

	}

	/**
	 * Force Plugin CSS if requested
	 */
	public function tif_generate_css_from_fo( $custom_css = false ) {

		if ( ! tif_is_writable() )
			return;

		$tif_dir = $this->tif_plugin_assets_dir();

		if ( ( isset( $_POST['tif_generate_css_nonce_field'] )
			&& wp_verify_nonce( $_POST['tif_generate_css_nonce_field'], 'tif_generate_css' )
			&& current_user_can( 'administrator' ) )
			)
			$this->tif_create_slider_css();

		if ( ! file_exists( $tif_dir['basedir'] . '/assets/css/style.css' )
			|| $custom_css
			)
			$this->tif_create_slider_css( $custom_css );

	}

	/**
	 * Add Slider JS to tif-main.js if configured
	 */
	public function tif_get_slider_js( $js ) {

		$tif_generated = $this->tif_get_generated_assets();

		if(    class_exists ( 'Themes_In_France' )
			&& ! in_array( 'js', $tif_generated )
			)
			return $js;

		// Prevent a notice
		$tif_js = array();
		$tif_js_build = '';

		// Array of css files
		$tif_js[] = TIF_SLIDER_DIR . 'assets/js/script.js';

		// Loop the css Array
		global $wp_filesystem;
		foreach ( $tif_js as $js_file ) {
			$tif_js_build .= $wp_filesystem->get_contents( $js_file );
		}

		// return the generated css
		$js .= $tif_js_build;

		return $js;

	}


}

return new Tif_Slider_Init();
