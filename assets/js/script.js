//
// Slider js
//

const tifSlider = document.getElementById("tif-slider");
tifSlider.classList.remove( 'no-js' );
tifSlider.classList.add( 'js' );

const tifSliderRemovedElements = document.querySelectorAll('.remove-if-js');
tifSliderRemovedElements.forEach(function (removedElement) {
	removedElement.remove();
});

let TifSlider = function ( id ){
	this.slider = document.getElementById( id );
	this.slideList = this.slider.getElementsByClassName('slide-list')[0];
	this.slideListItems = this.slider.getElementsByClassName('slide-item');
	this.slideWidth = this.slideListItems[0].offsetWidth;
	this.slidesLength = this.slideListItems.length;
	// Means we're at slide 0 (Slide 1)
	this.current = 1;
	this.direction;
	this.animating = false;

	this.delay = slider_options.delay;
};

TifSlider.prototype = {
	constructor : TifSlider,
	init : function(){
		this.listenEvents();
		this.cloneFirstAndLastItem();
	},

	listenEvents : function(){
		let that = this;
		let arrowButtons = this.slider.getElementsByClassName('arrow-button');
		setInterval(function() {
			that.autoPlay(this.delay);
		}, this.delay);
		for (let i = 0; i < arrowButtons.length; i++) {
			arrowButtons[i].addEventListener('click', function(){
				that.clickArrowButton( this );
			});
		};
		let pagerItems = this.slider.getElementsByClassName('pager-item');
		for (let i = 0; i < pagerItems.length; i++){
			pagerItems[i].addEventListener('click', function(){
				that.clickPagerItem( this );
			});
		};
	},

	cloneFirstAndLastItem : function(){
		let firstSlide = this.slideListItems[0];
		let lastSlide = this.slideListItems[ this.slidesLength - 1 ];
		let firstSlideClone = firstSlide.cloneNode( true );
		let lastSlideClone = lastSlide.cloneNode( true );

		// Remove data-slide-index for pager items to choose correct target
		firstSlideClone.removeAttribute('data-slide-index');
		firstSlideClone.setAttribute("aria-hidden", "true");
		firstSlideClone.classList.remove( 'current' );

		lastSlideClone.removeAttribute('data-slide-index');
		lastSlideClone.setAttribute("aria-hidden", "true");
		lastSlideClone.classList.remove( 'current' );

		this.slideList.appendChild( firstSlideClone );
		this.slideList.insertBefore( lastSlideClone, firstSlide );
	},

	autoPlay : function( el ){
		if( this.delay != "stop" ){
			let direction = "next";
			let pos = parseInt( this.slideList.style.left ) || 0;
			pos = pos === -600 ? 0 : pos;
			let newPos;
			// direction will be added to current slide number
			this.direction = direction === 'prev' ? -1 : 1;
			newPos = pos + ( -1 * 100 * this.direction );
			if( !this.animating ) {
				this.slideTo(this.slideList, function( progress ){
					return Math.pow(progress, 2);
				}, pos, newPos, 500);
				// Update current slide number
				this.current += this.direction;
			}
		}
	},

	currentSlide : function( current ){
		const checkPager = document.querySelectorAll('[data-slide-index]');
		checkPager.forEach(function ( element ) {
			element.classList.remove( 'current' );

			if(element.tagName == 'li' || element.tagName == 'LI'){
				element.setAttribute("aria-hidden", "true");
			}

			if( element.getAttribute("data-slide-index") == current ) {
				element.classList.add( 'current' );
				if(element.tagName == 'li' || element.tagName == 'LI'){
					element.removeAttribute("aria-hidden");
				}
			}
		});
	},

	clickArrowButton : function( el ){
		let direction = el.getAttribute('data-direction');
		let pos = parseInt( this.slideList.style.left ) || 0;
		let newPos;
		// direction will be added to current slide number
		this.direction = direction === 'prev' ? -1 : 1;
		newPos = pos + ( -1 * 100 * this.direction );
		if( !this.animating ) {
			this.slideTo(this.slideList, function( progress ){
				return Math.pow(progress, 2);
			}, pos, newPos, 500);
			// Update current slide number
			this.current += this.direction;
		}
		this.delay = 'stop';
	},

	clickPagerItem : function( el ){
		let slideIndex = el.getAttribute('data-slide-index');
		let targetSlide = this.slider.querySelector('.slide-item[data-slide-index="' + slideIndex +'"]');
		let pos = parseInt( this.slideList.style.left ) || 0;
		let newPos = Math.round( targetSlide.offsetLeft / targetSlide.offsetWidth ) * 100 * -1;

		if( !this.animating && pos !== newPos ){
			this.slideTo(this.slideList, function( progress ){
				return Math.pow(progress, 2);
			}, pos, newPos, 500);
			// Update current slide number
			this.current = parseInt(slideIndex) + 1;
		}
		this.delay = 'stop';
	},

	slideTo : function( element, deltaFunc, pos, newPos, duration ){
	this.animating = true;
	this.nextSlide = Math.abs(newPos / 100);
	this.nextSlide = this.nextSlide > this.slidesLength ? 0 : this.nextSlide - 1;
	this.nextSlide = this.nextSlide < 0 ? this.slidesLength - 1 : this.nextSlide;
	// console.log('next:'+this.nextSlide);

	this.oldSlide = Math.abs(pos / 100);
	this.oldSlide = this.oldSlide > this.slidesLength ? 0 : this.oldSlide - 1;
	this.oldSlide = this.oldSlide < 0 ? this.slidesLength - 1 : this.oldSlide;
	// console.log('old:'+this.oldSlide);

	const slidesList = document.querySelector('ul.slide-list');
	slidesList.classList.add( 'in-transition' );

	this.animate({
		delay: 20,
		duration: duration || 1000,
		deltaFunc: deltaFunc,
		step: function( delta ){

			let direction = pos > newPos ? 1 : -1
			let styleLeft = pos  + Math.abs(newPos - pos) * delta * direction * -1;
			element.style.left = styleLeft + '%';
			if(styleLeft == newPos){
				slidesList.classList.remove( 'in-transition' );
			}

			// PREV
			// Direction: -1
			// Pos = -600
			// newPos = 0
			// Ex: Slide 4 (0px) <- Slide 1 (-600px)
			//element.style.left = -600  + Math.abs(0 - (-600)) * 0.021 * -1 * -1+ 'px';

			// NEXT
			// Direction: +1
			// Pos = -600
			// newPos = -1200
			// Next: Slide 1 (-600px) -> Slide 2 (-1200px)
			// element.style.left = -600  + Math.abs( -600 - (-1200) ) * 0.021 * 1 * -1 + 'px';

		}
	});
	this.currentSlide(this.nextSlide);
	},

	animate : function( opts ){
		let that = this;
		let start = new Date();
		let id = setInterval(function(){
			let timePassed = new Date - start;
			let progress = timePassed / opts.duration;

			if( progress > 1 ) {
				progress = 1;
			}
			let delta = opts.deltaFunc( progress );
			opts.step( delta );

			if( progress === 1 ){
				clearInterval( id );
				that.animating = false;
				that.checknextSlide();
			}
		}, opts.delay || 10 );
	},

	checknextSlide : function( ){
		let cycle = false;
		//this.current += this.direction;
		// Are we at the cloned slides?
		cycle = !!( this.current === 0 || this.current > this.slidesLength )
		if ( cycle ) {
			// update current in order to adapt new slide list
			// we'll use current value to relocate slide list
			this.current = ( this.current === 0 ) ? this.slidesLength : 1;
			// For 4 x 600px slides,
			// left pos will be either -600px (first slide clone -> first slide)       // or -2400px (last slide clone -> fourth slide)
			this.slideList.style.left = ( -1 * this.current * 100 ) + '%';
		}

	}
};

document.addEventListener('DOMContentLoaded', function(){
	new TifSlider('tif-slider').init();
})

// @credit https://codepen.io/otuzel/pen/ObKxoE
// @credit https://output.jsbin.com/ufoceq/8/
