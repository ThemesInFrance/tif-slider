<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
* Slider front render function
*/
function tif_render_slider() {

	if ( is_paged() )
	return;

	if( null == tif_get_option( 'plugin_slider', 'tif_init,enabled', 'checkbox' ) ) {

		if ( current_user_can( 'customize' ) ) {

			?>

			<section id="tif-slider" class="tif-slider">

				<div class="inner tif-boxed">

					<div class="tif-alert tif-alert--warning">

						<?php

						if ( is_customize_preview() ) {
							echo '<span id="tif-slider-disabled"></span>';
							printf(
								'<p>%s</p>',
								esc_html__( 'Slider is disabled.', 'tif-slider' )
							);
						}

						if ( ! is_customize_preview() )
						printf(
							'<p><a href="%2$s">%1$s</a>.</p>',
							esc_html__( 'Slider can be enabled in the customizer', 'tif-slider' ),
							admin_url( 'customize.php?autofocus[section]=tif_plugin_slider_panel_settings_section' )
						);

						?>

					</div>

				</div><!-- .inner -->

			</section><!-- #tif-slider -->

			<?php

		}

		return;

	}

	if( null == tif_get_option( 'plugin_slider', 'tif_init,enabled', 'checkbox' ) )
		return;

	$options             = tif_get_option( 'plugin_slider', 'tif_options', 'array' );
	$layout              = tif_get_option( 'plugin_slider', 'tif_layout', 'array' );
	$colors              = tif_get_option( 'plugin_slider', 'tif_colors', 'array' );
	$content             = tif_get_option( 'plugin_slider', 'tif_slider_content', 'array' );
	$object              = tif_sanitize_cover_fit( $layout['cover_fit'] );
	$features            = tif_sanitize_multicheck( $layout['features']);

	$title_bgcolor       = $colors['title_bgcolor'];
	$title_bgcolor       = is_array( $title_bgcolor ) ? $title_bgcolor : explode( ',', $title_bgcolor );
	$title_has_bgcolor   = $title_bgcolor[2] > 0 ? ' has-background' : null;

	$subtitle_bgcolor    = $colors['subtitle_bgcolor'];
	$subtitle_bgcolor    = is_array( $subtitle_bgcolor ) ? $subtitle_bgcolor : explode( ',', $subtitle_bgcolor );
	$subtitle_has_bgcolor= $subtitle_bgcolor[2] > 0 ? ' has-background' : null;

	$excerpt_bgcolor     = $colors['excerpt_bgcolor'];
	$excerpt_bgcolor     = is_array( $excerpt_bgcolor ) ? $excerpt_bgcolor : explode( ',', $excerpt_bgcolor );
	$excerpt_has_bgcolor = $excerpt_bgcolor[2] > 0 ? ' has-background' : null;

	$how_many            = max( 1, min( 5, (int)$options['how_many'] ) );
	$sticky              = (string)$options['sticky'];

	$width               = absint( $layout['thumb_width'] );
	$ratio               = str_replace( '.', ',', $layout['thumb_ratio']);
	$ratio               = ! is_array( $ratio ) ? explode( ',', $ratio ) : $ratio;
	$ratio               = ( (int)$ratio[1] / (int)$ratio[0] );
	$height              = round( $width * $ratio );
	$tif_num_slider      = (int)0;

	switch ( $options['content'] ) {

		case 'category':
		case 'posts':

			// ignore sticky post
			$args        = array(
				'ignore_sticky_posts' => true
			);
			$is_sticky   = get_option( 'sticky_posts' );

			// Sticky posts only
			if ( $sticky == 'only' )
				$args    = array( 'post__in' => $is_sticky );

			// Exlude sticky posts
			if ( $sticky == 'exclude' )
				$args    = array( 'post__not_in' => $is_sticky );

			/**
			* @link https://developer.wordpress.org/reference/functions/query_posts/
			*/
			$args = array_merge(
				$args,
				array(
					'cat'            => ( $options['content'] == 'category' ? ( is_array( $options['category'] ) ? implode( ',' , $options['category'] ) : $options['category'] ) : null ),
					'posts_per_page' => (int)$how_many,
					'post_type'      => 'post',
					'post_status'    => 'publish',
					'meta_key'       => ( $options['has_thumbnail'] ? '_thumbnail_id' : false ),
				)
			);

			$i = 1;
			// query_posts( $args );
			$tif_the_query = new WP_Query( $args );
			while( $tif_the_query->have_posts() ): $tif_the_query->the_post();

			global $post;

			$title[$i]      = tif_sanitize_wrap_attr( $content['slider_' . $i]['title'] );
			$title[$i][0]   = get_the_title();
			$title_link[$i] = get_permalink();
			$post_id[$i]    = $post->ID;
			$thumb_id[$i]   = get_post_thumbnail_id( $post->ID );
			$excerpt[$i]    = tif_sanitize_wrap_attr( $content['slider_' . $i]['excerpt'] );
			$excerpt[$i][0] = get_the_excerpt();

			$btn1[$i]       = null;
			$btn1_link[$i]  = null;

			$btn2[$i]       = null ;
			$btn2_link[$i]  = null ;

			$tif_num_slider = $i;

			++$i;

		endwhile;

		// Reset Query
		wp_reset_postdata();

	break;

	default:

		for( $i = 1; $i <= (int)$how_many; ++$i ) {

			$title[$i]      = isset( $content['slider_' . $i]['title'] )      ? tif_sanitize_wrap_attr( $content['slider_' . $i]['title'] )    : false;
			$title_link[$i] = isset( $content['slider_' . $i]['title_link'] ) ? esc_url_raw( $content['slider_' . $i]['title_link'] )          : false;
			$subtitle[$i]   = isset( $content['slider_' . $i]['subtitle'] )   ? tif_sanitize_wrap_attr( $content['slider_' . $i]['subtitle'] ) : false;
			$thumb_id[$i]   = isset( $content['slider_' . $i]['thumbnail_id'] )   ? (int)$content['slider_' . $i]['thumbnail_id']                     : false ;
			$excerpt[$i]    = isset( $content['slider_' . $i]['excerpt'] )    ? tif_sanitize_wrap_attr( $content['slider_' . $i]['excerpt']  )  : false;
			$btn1[$i]       = isset( $content['slider_' . $i]['btn1_txt'] )   ? esc_html( $content['slider_' . $i]['btn1_txt'] )               : false;
			$btn1_link[$i]  = isset( $content['slider_' . $i]['btn1_link'] )  ? esc_url_raw( $content['slider_' . $i]['btn1_link'] )           : false;
			$btn2[$i]       = isset( $content['slider_' . $i]['btn2_txt'] )   ? esc_html( $content['slider_' . $i]['btn2_txt'] )               : false;
			$btn2_link[$i]  = isset( $content['slider_' . $i]['btn2_link'] )  ? esc_url_raw( $content['slider_' . $i]['btn2_link'] )           : false;

			if ( $thumb_id[1] )
				$tif_num_slider = $i;

			else
				$tif_num_slider = $thumb_id[$i] ? $tif_num_slider : $i ;

		}

	break;

	}

	$arrow = $inputs = null;
	$tif_num_slider = $tif_num_slider >= 5 ? 5 : (int)$tif_num_slider;

	for( $i = 1; $i <= $tif_num_slider ; ++$i ) {

		$checked              = ( $i == 1 ) ? ' checked="checked"' : $checked = '' ;
		$labels               = '<label for="bullet' . $i . '" class=" remove-if-js"></label>' . "\n";
		$inputs              .= "\n".'<input type="radio" id="bullet' . $i . '" name="bullets"' . $checked . ' class="hidden remove-if-js" />'. "\n" ;
		$inputs              .= in_array( "bullets", $features ) ? $labels . "\n" : null ;
		$arrow               .= $labels;

		$title_enabled[$i]    = ! empty( $title[$i][0] ) && $title[$i][0] ? true : false ;
		$subtitle_enabled[$i] = ! empty( $subtitle[$i][0] ) && $subtitle[$i][0] ? true : false ;
		$excerpt_enabled[$i]  = ! empty( $excerpt[$i][0] ) && $excerpt[$i][0] ? true : false ;
		$btn1_enabled[$i]     = null != $btn1[$i]  ? true : false ;
		$btn2_enabled[$i]     = null != $btn2[$i]  ? true : false ;

	}

	?>
	<section id="tif-slider" class="tif-slider <?php echo ( $tif_num_slider >= 2 ? 'has-arrow' : null ) ?> no-js">

		<div class="inner <?php echo ( null != $layout['wide_alignment'] ? tif_sanitize_css( $layout['wide_alignment'] ) : false ); ?>">

			<?php

			if ( is_customize_preview() )
				echo '<span id="edit-tif-slider"></span>';

			if ( $tif_num_slider >= 2 )
				echo $inputs . "\n";

			?>

			<ul class="slide-list" <?php if( $tif_num_slider >= 2) echo 'style="left: -100%"' ?>>

				<?php

				for( $i = 1; $i <= $tif_num_slider; ++$i ) {

					$output = null;

					$link_open	= ( null != $title_link[$i] ) ? '<a href="' . $title_link[$i] . '">' : null ;
					$link_close	= ( null != $title_link[$i] ) ? '</a>' : null ;

					if ( $title_enabled[$i] || $subtitle_enabled[$i] )
					$output .= '<header>';

					if ( $title_enabled[$i] ){

						$output .= '<' . esc_attr( $title[$i][1] ) . ' class="entry-title slider-title ' .
						esc_attr( isset( $title[$i][2] ) && $title[$i][2] ? $title[$i][2] : null ) .
						'"><span ' . tif_parse_attr( array( 'class' => $title_has_bgcolor ) ) . '>';
						$output .= wp_kses( $link_open . $title[$i][0] . $link_close, tif_allowed_html() );
						$output .= '</span></' . esc_attr( $title[$i][1] ) . '>';

					}

					if( $subtitle_enabled[$i] ){

						$output .='<' . esc_attr( $subtitle[$i][1] ) . ' class="entry-sub-title slider-sub-title ' .
						esc_attr( isset( $subtitle[$i][2] ) && $subtitle[$i][2] ? $subtitle[$i][2] : null ) .
						'"><span ' . tif_parse_attr( array( 'class' => $subtitle_has_bgcolor ) ) . '>';
						$output .= wp_kses( $subtitle[$i][0], tif_allowed_html() );
						$output .='</span></' . esc_attr( $subtitle[$i][1] ) . '>';

					}

					if ( $title_enabled[$i] || $subtitle_enabled[$i] )
					$output .= '</header>';

					if( $excerpt_enabled[$i] ){

						$output .='<' . esc_attr( $excerpt[$i][1] ) . ' class="entry-content ' . $excerpt_has_bgcolor .
						esc_attr( isset( $excerpt[$i][2] ) && $excerpt[$i][2] ? $excerpt[$i][2] : null ) .
						'">';
						$output .= wp_kses( $excerpt[$i][0], tif_allowed_html() );
						$output .='</' . esc_attr( $excerpt[$i][1] ) . '>';

					}

					if( $btn1_enabled[$i] || $btn2_enabled[$i] ){

						$output .= '<footer class="flex gap-5">';

						if( $btn1_enabled[$i] )
						$output .= '<a href="' . esc_url( $btn1_link[$i] ) . '" class="btn ' . esc_attr( $layout['btn1_class'] ) . '">' . esc_html( $btn1[$i] ) . '</a>';

						if ( $btn2_enabled[$i] )
						$output .= '<a href="' . esc_url( $btn2_link[$i] ) . '" class="btn ' .  esc_attr( $layout['btn2_class'] ). '">' . esc_html( $btn2[$i] ) . '</a>';

						$output .= '</footer>';


					}

					$layout_attr = tif_sanitize_multicheck( $layout['layout'] );
					$layout_attr = tif_get_loop_attr(
						array(
							$layout_attr[0],
							2,
							null,
							null,
							null,
							null,
							( isset( $layout_attr[1] ) ? $layout_attr[1] : false ),
							( isset( $layout_attr[2] ) ? $layout_attr[2] : false ),
						),
						array(
							'has_overlay'    => true,
						)
					);

					$thumb_attr = array(
						'thumbnail'         => array(
							'size'              => 'tif-slider',
							'sizes'             => array(
								'width'             => (int)$width,
								'height'            => (int)$height,
								'ratio'             => $layout['thumb_ratio'],
							),
							'id'                => ( $thumb_id[$i] ? (int)$thumb_id[$i] : false ),
							'url'               => ( ! $thumb_id[$i] && ! $thumb_id[1] && $options['content'] == 'selected' ? esc_url( TIF_SLIDER_IMG_URL . 'slide' . $i . '.jpg' ) : false ),
							'object'            => array(
								'fit'               => $object[0],
								'position'          => $object[1],
								'height'            => tif_get_length_value( $object[2] . ',' . $object[3] ),
								'fixed'             => isset( $object[4] ) && $object[4] ? (bool)$object[4] : false,
							),
							'attr'              => array(
								'class'             => false,
								'style'             => false,
								'alt'               => (isset( $title[$i][0] ) && null != $title[$i][0] ? $title[$i][0] : false),
								'title'             => (isset( $title[$i][0] ) && null != $title[$i][0] ? $title[$i][0] : false),
							),
						),
					);

					?>

					<li class="slide-item current" data-slide-index="<?php echo (int)($i - 1) ?>">

						<article <?php echo tif_parse_attr( $layout_attr['container']['attr'] ) ?>>

							<?php

							// Open Inner if there is one
							if ( isset( $layout_attr['inner']['wrap'] ) && $layout_attr['inner']['wrap'] )
							 	echo '<' . esc_attr( $layout_attr['inner']['wrap'] ) . tif_parse_attr( $layout_attr['inner']['attr'] ) . '>' . "\n\n" ;

							// if( $options['content'] == 'selected' ) {

								echo tif_attachment_thumbnail( $thumb_attr );

							// } else {;
							//
							// 	echo tif_get_the_post_thumbnail(
							// 		(int)$thumb_id[$i],
							// 		(int)$post_id[$i],
							// 		'tif-slider',
							// 		$thumb_attr
							// 	);
							//
							// }

							if( null != $output ) {

								echo '<div ' . tif_parse_attr( $layout_attr['post']['wrap_content']['attr'] ) . '>' . "\n";
								echo $output;
								echo '</div>' . "\n";

							}

							// Close Inner if there is one
							if ( isset( $layout_attr['inner']['wrap'] ) && $layout_attr['inner']['wrap'] )
							 	echo '</' . esc_attr( $layout_attr['post']['inner']['wrap'] ) . '>' . "\n\n"

							?>

						</article>

					</li>

				<?php
				}

				?>

			</ul>

			<script>
			const tifSlide = document.querySelector('.slide-list');
			<?php
			if ( (int)$tif_num_slider > 1 )
				echo 'tifSlide.style.width = ' . ( (int)$tif_num_slider + 2 ) * 100 . '%';
			else
				echo 'tifSlide.style.width = 100%';
			?>
			</script>

			<?php

			$controls  = null;

			if( in_array( "arrows", $features ) ) {

				$controls .= '<div id="arrows" class="arrows">';
				$controls .= $arrow;
				$controls .= '</div><!-- #arrows -->';

			}

			if ( $tif_num_slider >= 2 ) {

				$controls .= '<div class="slider-controls">';

				if( in_array( "arrows", $features ) ) {

					$controls .= '<button type="button" data-direction="prev" class="arrow-button prev"></button>';
					$controls .= '<button type="button" data-direction="next " class="arrow-button next"></button>';

				}

				if( in_array( "bullets", $features ) ) {

					$controls .= '<div class="slider-pager">';

					for( $i = 0; $i <= $tif_num_slider - 1; ++$i ) {

						$controls .= '<button class="pager-item' . ( $i == 0 ? ' current' : null ) . '" type="button" data-slide-index="' . (int)$i . '">';
						$controls .= '<span class="screen-reader--text">' . (int)($i + 1) . '</span>';
						$controls .= '</button>';

					}

					$controls .= '</div><!-- .slider-pager -->';
				}

				$controls .= '</div><!-- .slider-controls -->';

				echo $controls;

			}

			?>

		</div><!-- .inner -->

	</section><!-- #slider -->

	<?php

}
