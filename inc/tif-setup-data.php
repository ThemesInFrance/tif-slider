<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Slider setup data
 */

function tif_plugin_slider_setup_data() {

	$boxed_width = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : '1200';

	return $tif_slider_setup_data = array(

		'tif_tabs'                  => 1,

		// Settings
		'tif_init'                  => array(
			'enabled'                   => true,
			'customizer_enabled'        => 1,
			'generated'                 => array( null ),
			'css_enabled'               => null,
			'capabilities'              => null,
			'custom_css'                => null,
		),

		'tif_options'                => array(
			// ... SECTION // Slider settings ..................................
			'how_many'                  => 3,
			'delay'                     => 5000,
			'animation'                 => 'slide',
			'content'                   => 'selected',
			'sticky'                    => 'exclude',
			'has_thumbnail'             => 1,

		),

		// ... SECTION // Slider layout ........................................
		'tif_layout'                => array(
			'layout'                    => 'cover',
			'boxed_width'               => $boxed_width,
			'wide_alignment'            => 'tif_boxed',
			'cover_fit'                 => array(
				'cover',
				'center center',
				'50',
				'vh',
				// (bool) fixed
			),
			'features'                  => array(
				'arrows',
				'bullets',
			),
			'alignment'                 => array(
				'align_items'               => 'start',
				'justify_content'           => 'center',
				'gap'                       => 'gap_medium'
			),
			'btn1_class'                => 'btn--default',
			'btn2_class'                => 'btn--default',

			'thumb_ratio'               => '16,9',
			'thumb_width'               => 1200,
			'thumb_hide'                => null,
		),

		// ... SECTION // Slider colors ........................................
		'tif_colors'                => array(
			'bgcolor'                   => 'none,normal,1',
			'overlay_bgcolor'           => '#000000,normal,.5',
			'title_bgcolor'             => '#000000,normal,0',
			'subtitle_bgcolor'          => '#000000,normal,0',
			'excerpt_bgcolor'           => '#000000,normal,0',
			// 'content_bgcolor'           => '#000000,normal,.5',
			'title_color'               => '#ffffff',
			'subtitle_color'            => '#ffffff',
			'excerpt_color'             => '#ffffff',
		),

		// ... SECTION // Slider Content #1 to #5 ..............................
		'tif_slider_content'=> array(

			// ... SECTION // Slider #1 ........................................
			'slider_1'                  => array(
				'title'                     => array(
					esc_html__( 'Here is your title #1', 'tif-slider' ),
					'h2',
				),
				'title_link'                => '#',
				'subtitle'                  => array(
					esc_html__( 'Sub title #1', 'tif-slider' ),
					'h3',
				),
				'excerpt'                   => array(
					esc_html__( 'Praesent ultricies dapibus ante, ac ultrices purus pulvinar eget. Donec vitae diam congue, sodales erat at, porttitor orci. Proin euismod nibh felis, eu tristique diam dapibus vestibulum.', 'tif-slider' ),
					'span',
				),
				'thumbnail_id'              => null,
				'btn1_txt'                  => 'Decaseconds / Flickr - CC BY-NC 2.0',
				'btn1_link'                 => 'https://www.flickr.com/photos/decaseconds/30534804756/',
				'btn2_txt'                  => null,
				'btn2_link'                 => null,
			),

			// ... SECTION // Slider #2 ........................................
			'slider_2'                  => array(
				'title'                     => array(
					esc_html__( 'Here is your title #2', 'tif-slider' ),
					'h2',
				),
				'title_link'                => '#',
				'subtitle'                  => array(
					esc_html__( 'Sub title #2', 'tif-slider' ),
					'h3',
				),
				'excerpt'                   => array(
					esc_html__( 'Cras consectetur, magna nec interdum posuere, urna nunc sollicitudin nisl, sit amet cursus mi eros non quam. Morbi id magna sit amet massa fringilla rutrum in ac turpis.', 'tif-slider' ),
					'span'
				),
				'thumbnail_id'              => null,
				'btn1_txt'                  => 'Mizzou CAFNR / Flickr - CC BY-NC 2.0',
				'btn1_link'                 => 'https://www.flickr.com/photos/cafnr/10452730416/',
				'btn2_txt'                  => null,
				'btn2_link'                 => null,
			),

			// ... SECTION // Slider #3 ........................................
			'slider_3'                  => array(
				'title'                     => array(
					esc_html__( 'Here is your title #3', 'tif-slider' ),
					'h2',
				),
				'title_link'                => '#',
				'subtitle'                  => array(
					esc_html__( 'Sub title #3', 'tif-slider' ),
					'h3',
				),
				'excerpt'                   => array(
					esc_html__( 'Sed ac leo vulputate, egestas ligula ac, egestas nisi. Pellentesque a pharetra orci, et scelerisque dolor.', 'tif-slider' ),
					'span'
				),
				'thumbnail_id'              => null,
				'btn1_txt'                  => 'Gael Varoquaux / Flickr - CC BY 2.0',
				'btn1_link'                 => 'https://www.flickr.com/photos/gaelvaroquaux/30752374406/',
				'btn2_txt'                  => null,
				'btn2_link'                 => null,
			),

			// ... SECTION // Slider #4 ........................................
			'slider_4'                  => array(
				'title'                     => array(
					esc_html__( 'Here is your title #4', 'tif-slider' ),
					'h2',
				),
				'title_link'                => '#',
				'subtitle'                  => array(
					esc_html__( 'Sub title #4', 'tif-slider' ),
					'h3',
				),
				'excerpt'                   => array(
					esc_html__( 'Phasellus mollis nunc lorem, id pharetra nibh viverra id. Praesent vitae ex ut tellus tincidunt consequat. Nam sollicitudin augue eu diam molestie, vel faucibus turpis dapibus.', 'tif-slider' ),
					'span'
				),
				'thumbnail_id'              => null,
				'btn1_txt'                  => 'Sigfrid Lundberg / Flickr - CC BY-SA 2.0',
				'btn1_link'                 => 'https://www.flickr.com/photos/sigfridlundberg/22451015162/',
				'btn2_txt'                  => null,
				'btn2_link'                 => null,
			),

			// ... SECTION // Slider #5 ........................................
			'slider_5'                  => array(
				'title'                     => array(
					esc_html__( 'Here is your title #5', 'tif-slider' ),
					'h2',
				),
				'title_link'                => '#',
				'subtitle'                  => array(
					esc_html__( 'Sub title #5', 'tif-slider' ),
					'h3',
				),
				'excerpt'                   => array(
					esc_html__( 'Aliquam erat volutpat. Pellentesque sit amet lectus tempus, volutpat mi ac, maximus lorem. Nam feugiat at ex in euismod. Nunc eros purus, placerat sit amet laoreet a, rhoncus quis felis.', 'tif-slider' ),
					'span'
				),
				'thumbnail_id'              => null,
				'btn1_txt'                  => 'Virtualwayfarer / Flickr - CC BY-NC 2.0',
				'btn1_link'                 => 'https://www.flickr.com/photos/virtualwayfarer/30565546368/',
				'btn2_txt'                  => null,
				'btn2_link'                 => null,

			),

		)

	);

}
