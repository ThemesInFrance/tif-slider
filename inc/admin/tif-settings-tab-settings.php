<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Activate', 'tif-slider' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Plugin Enabled', 'tif-slider' ),
		array(
			'type'            => 'checkbox',
			'is_admin'        => true,
			'value'           => 1,
			'checked'         => tif_get_option( 'plugin_slider', 'tif_init,enabled', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_init][enabled]'
	);

	$form->add_input( esc_html__( 'Customizer Enabled', 'tif-slider' ),
		array(
			'type'            => 'checkbox',
			'is_admin'        => true,
			'value'           => 1,
			'checked'         => tif_get_option( 'plugin_slider', 'tif_init,customizer_enabled', 'checkbox' ),
		),
		$tif_plugin_name . '[tif_init][customizer_enabled]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Assets', 'tif-slider' ) . '</legend>'
) );

	if ( class_exists ( 'Themes_In_France' ) ) {
		$form->add_input( esc_html__( 'Add to generated files', 'tif-slider' ),
			array(
				'type'			=> 'checkbox',
				'value'			=> tif_get_option( 'plugin_slider', 'tif_init,generated', 'multicheck' ),
				'checked'		=> tif_get_default( 'plugin_slider', 'tif_init,generated', 'multicheck' ),
				'options'		=> array(
					'css'			=> esc_html__( 'CSS', 'tif-slider' ),
					'js'			=> esc_html__( 'JS', 'tif-slider' ),
				),
			),
			$tif_plugin_name . '[tif_init][generated]'
		);
	}

	$form->add_input( esc_html__( 'CSS enabled', 'tif-slider' ),
		array(
			'type'            => 'radio',
			'is_admin'        => true,
			'checked'         => tif_get_option( 'plugin_slider', 'tif_init,css_enabled', 'radio' ),
			'options'         => array(
				''                => esc_html__( 'Plugin CSS (including custom css)', 'tif-slider' ),
				'custom'          => esc_html__( 'Custom CSS only', 'tif-slider' ),
			),
		),
		$tif_plugin_name . '[tif_init][css_enabled]'
	);

	$form->add_input( esc_html__( 'Custom CSS', 'tif-slider' ),
		array(
			'type'			=> 'textarea',
			'value'			=> tif_get_option( 'plugin_slider', 'tif_init,custom_css', 'textarea' ),
		),
		$tif_plugin_name . '[tif_init][custom_css]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );

$form->add_input( 'html' . $count++ , array(
	'type' => 'html',
	'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'Access to settings', 'tif-slider' ) . '</legend>'
) );

	$form->add_input( esc_html__( 'Allowed roles', 'tif-slider' ),
		array(
			'type'            => 'checkbox',
			'is_admin'        => true,
			'value'           => tif_get_option( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ),
			'checked'         => tif_get_default( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ),
			'options'         => tif_get_wp_roles(),
		),
		$tif_plugin_name . '[tif_init][capabilities]'
	);

$form->add_input( 'html' . $count++, array(
	'type' => 'html',
	'value' => '</fieldset>'
) );
