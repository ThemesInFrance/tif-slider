<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_slider_options_page() {

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_slider', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-slider' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php _e( 'Slider', 'tif-slider' ) ?></h1>
		<p><?php _e( 'Create a beautiful slider in minutes on your homepage.', 'tif-slider' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-slider' );
			wp_nonce_field( 'tif_slider_action', 'tif_slider_nonce_field' );
			$tif_plugin_name = 'tif_plugin_slider';

			$form = new Tif_Form_Builder();

			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			$form->set_att( 'novalidate', false );
			$form->set_att( 'is_array', 'tif_plugin_slider' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_slider', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Settings', 'tif-slider' ),
				'dashicon' => 'dashicons-admin-settings',
				'first' => true,
			) );

			require_once 'tif-settings-tab-settings.php';

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_slider', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-slider' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			require_once 'tif-settings-tab-help.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
