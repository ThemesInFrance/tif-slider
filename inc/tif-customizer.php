<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! tif_get_option( 'plugin_slider', 'tif_init,customizer_enabled', 'checkbox' ) )
	return;

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

class Tif_Slider_Customize {

	/**
	 * This hooks into 'customize_register' ( available as of WP 3.4 ) and allows
	 * you to add new sections and controls to the Theme Customize screen.
	 *
	 * Note: To enable instant preview, we have to actually write a bit of custom
	 * javascript. See live_preview() for more.
	 *
	 * @see add_action( 'customize_register',$func )
	 * @param \WP_Customize_Manager $wp_customize
	 * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
	 */

	public static function register ( $wp_customize ) {

		/**
		 * Assigning plugin name
		 */
		$tif_plugin_name = 'tif_plugin_slider';

		// === PANEL // Slider PANEL ===========================================

		$wp_customize->add_panel(
			'tif_plugin_slider_panel',
			array(
				'capabitity'        => 'edit_theme_options',
				'priority'          => 1010,
				'title'             => esc_html__( 'Slider', 'tif-slider' )
			)
		);

		// ... SECTION // Slider settings ......................................

		$wp_customize->add_section(
			'tif_plugin_slider_panel_settings_section',
			array(
				'priority'          => 601,
				'title'             => esc_html__( 'Settings', 'tif-slider' ),
				'panel'             => 'tif_plugin_slider_panel'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_init,enabled', 'checkbox' ),
				'capability'        => 'edit_theme_options',
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_checkbox',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'section'           => 'tif_plugin_slider_panel_settings_section',
				'label'             => esc_html__( 'Slider enabled', 'tif-slider' ),
				'type'              => 'checkbox',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'selector' => '#tif-slider-disabled',
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][how_many]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_options,how_many', 'absint' ),
				'type'              => 'option',
				'sanitize_callback' => 'absint',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_options][how_many]',
			array(
				'section'           => 'tif_plugin_slider_panel_settings_section',
				'label'             => esc_html__( 'Number of slides', 'tif-slider' ),
				'type'              => 'number',
				'input_attrs'       => array(
					'min'               => '1',
					'max'               => '5',
					'step'              => '1',
				),
			)
		);

		// Slider Animation duration
		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][delay]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_options,delay', 'absint' ),
				'type'              => 'option',
				'sanitize_callback' => 'absint',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_options][delay]',
			array(
				'section'           => 'tif_plugin_slider_panel_settings_section',
				'label'             => esc_html__( 'Slide Duration (in milliseconds)', 'tif-slider' ),
				'description'       => esc_html__( '"0" to disable auto play', 'tif-slider' ),
				'type'              => 'number',
				'input_attrs'       => array(
					'min'               => '0',
					'max'               => '10000',
					'step'              =>'250',
				),
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][content]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_options,content', 'key' ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_select',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_options][content]',
			array(
				'section'           => 'tif_plugin_slider_panel_settings_section',
				'label'             => esc_html__( 'Select Slider type', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => array(
					'selected'          => esc_html__( 'Selected Slider', 'tif-slider' ),
					'category'          => esc_html__( 'Category(ies)', 'tif-slider' ),
					'posts'             => esc_html__( 'Posts', 'tif-slider' ),
				)
			)
		);
		$wp_customize->selective_refresh->add_partial(
			$tif_plugin_name . '[tif_options][content]',
			array(
				'selector' => '#edit-tif-slider',
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][category]',
			array(
				'default'           => get_option( 'default_category' ),
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_dropdown_category',
				'type'              => 'option',
			)
		);
		$wp_customize->add_control(
		new Tif_Customize_Category_Dropdown_Control(
			$wp_customize,
			$tif_plugin_name . '[tif_options][category]',
				array(
					'section'       => 'tif_plugin_slider_panel_settings_section',
					'label'         => esc_html__( 'Select category(ies) for the slider', 'tif-slider' ),
					'settings'      => $tif_plugin_name . '[tif_options][category]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][sticky]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_options,sticky', 'select' ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_select',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_options][sticky]',
			array(
				'section'           => 'tif_plugin_slider_panel_settings_section',
				'label'             => esc_html__( 'Sticky posts', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => array(
					'exclude'           => esc_html__( 'Exclude Sticky posts', 'tif-slider' ),
					'include'           => esc_html__( 'Include Sticky posts', 'tif-slider' ),
					'only'              => esc_html__( 'Sticky posts only', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_options][has_thumbnail]',
			array(
				'default'			=> tif_get_default( 'plugin_slider', 'tif_options,has_thumbnail', 'checkbox' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_checkbox'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_options][has_thumbnail]',
			array(
				'section'			=> 'tif_plugin_slider_panel_settings_section',
				'type'				=> 'checkbox',
				'label'				=> esc_html__( 'Post with thumbnail only', 'tif-slider' ),
			)
		);

		// ... SECTION // Slider layout ........................................

		$wp_customize->add_section(
			'tif_plugin_slider_panel_layout_section',
			array(
				'priority'          => 601,
				'title'             => esc_html__( 'Layout', 'tif-slider' ),
				'panel'             => 'tif_plugin_slider_panel'
			)
		);


		$wp_customize->add_setting(
			'tif_plugin_slider_panel_layout_section_layout_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_layout_section_layout_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_layout_section',
					'label'         => esc_html__( 'Layout', 'tif-slider' ),
				)
			)
		);

		$tif_layout = tif_get_default( 'plugin_slider', 'tif_layout,layout', 'multicheck' );
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][layout]',
			array(
				'default'           => (array)$tif_layout,
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Main_Layout_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_layout][layout]',
				array(
					'section'           => 'tif_plugin_slider_panel_layout_section',
					'label'             => esc_html__( 'Slider layout', 'tif-slider' ),
					'choices'           => array(
						0	=> array(
							'id'         => 'layout',
							'label'         => false,
							'choices'           => array(
								'cover'               => array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-cover.png', esc_html_x( 'Default', 'Cover layout', 'tif-slider' ) ),
								// 'cover_grid_2'        => array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-cover-grid-2.png', esc_html_x( 'Cover (50/50)', 'Cover layout', 'tif-slider' ) ),
								'cover_media_text'    => array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-cover-media-text.png', esc_html_x( 'Media/Text', 'Cover layout', 'tif-slider' ) ),
								'cover_text_media'    => array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-cover-text-media.png', esc_html_x( 'Text/Media', 'Cover layout', 'tif-slider' ) ),
								// 'none'                => array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-none-square.png', esc_html_x( 'None', 'Cover layout', 'tif-slider' ) )
							),
						),
						1	=> array(
							'id'          => 'container_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-slider' ),
								esc_html( isset( $tif_layout[1] ) ? $tif_layout[1] : null ),
							)
						),
						2	=> array(
							'id'          => 'post_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-slider' ),
								esc_html( isset( $tif_layout[2] ) ? $tif_layout[2] : null ),
							)
						),
					),
					'settings'          => $tif_plugin_name . '[tif_layout][layout]'
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_slider_panel_settings_section_images_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_settings_section_images_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_layout_section',
					'label'         => esc_html__( 'Images', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][wide_alignment]',
			array(
				'default'			=> tif_get_default( 'plugin_slider', 'tif_layout,wide_alignment', 'key' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_layout][wide_alignment]',
				array(
					'section'			=> 'tif_plugin_slider_panel_layout_section',
					'label'				=> esc_html__( 'Alignment', 'tif-slider' ),
					'choices'			=> array(
						'tif_boxed'			=> array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-align-center.png', esc_html__( 'Align center', 'tif-slider' ) ),
						'alignwide'			=> array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-align-wide.png', esc_html__( 'Wide width', 'tif-slider' ) ),
						'alignfull'			=> array( TIF_SLIDER_ADMIN_IMAGES_URL . '/layout-align-full.png', esc_html__( 'Full width', 'tif-slider' ) ),
					),
					'settings'		=> $tif_plugin_name . '[tif_layout][wide_alignment]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][thumb_width]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_layout,thumb_width', 'absint' ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_float',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][thumb_width]',
			array(
				'section' => 'tif_plugin_slider_panel_layout_section',
				'label' => esc_html__( 'Thumbnail width', 'tif-slider' ),
				'type'              => 'number',
				'input_attrs'       => array(
					'min' => '1000',
					'max' => '2000',
					'step' => '100',
				),
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][thumb_ratio]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_layout,thumb_ratio', 'text' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_string',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][thumb_ratio]',
			array(
				'section'           => 'tif_plugin_slider_panel_layout_section',
				'label'             => esc_html__( 'Thumbnail ratio', 'tif-slider' ),
				'type'              => 'select',
				'description'       => sprintf( '%s<br /><a href="%s" class="external-link" target="_blank" rel="noreferrer noopener">%s</a>',
				esc_html__( 'If uncropped, images will display using the aspect ratio in which they were uploaded', 'tif-slider' ),
				esc_url( __( 'https://en.wikipedia.org/wiki/Aspect_ratio_(image)', 'tif-slider' ) ),
				esc_html__( 'More details on image ratio', 'tif-slider' )
				),
				'choices'           => array(
					/**
					 * @link https://en.wikipedia.org/wiki/Aspect_ratio_(image)
					 */
					'uncropped'         => esc_html__( 'Original format (uncropped) ', 'tif-slider' ),
					'1,1'               => '1:1',
					'6,5'               => '6:5',
					'4,3'               => '4:3',
					'3,2'               => '3:2',
					'5,3'               => '5:3',
					'16,9'              => '16:9',
					'2,1'               => '2:1',
					'3,1'               => '3:1',
					'4,1'               => '4:1',
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][thumb_hide]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_layout,thumb_hide', 'key' ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][thumb_hide]',
			array(
				'section'           => 'tif_plugin_slider_panel_layout_section',
				'label'             =>  esc_html__( 'Hide thumbnail', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => array(
					''                  => esc_html__( 'Always keep visible', 'tif-slider' ),
					'small'             => esc_html__( 'On small screen', 'tif-slider' ),
					'medium'            => esc_html__( 'On medium screen', 'tif-slider' ),
					'large'             => esc_html__( 'On large screen', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][cover_fit]',
			array(
				'default'			=> tif_get_default( 'plugin_slider', 'tif_layout,cover_fit', 'cover_fit' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_cover_fit'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Image_Cover_Fit_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_layout][cover_fit]',
				array(
					'section'			=> 'tif_plugin_slider_panel_layout_section',
					// 'label'				=> esc_html__( 'Change thumbnail alignment', 'tif-slider' ),
					'description'		=> array(
						'main'				=> false,
						'preset'			=> false,
						'position'			=> false,
						'height'			=> esc_html( '0 to use original ratio', 'tif-slider' ),
					),
					'choices'			=> array(
						'default'			=> esc_html__( 'Original', 'tif-slider' ),
						'cover'				=> esc_html__( 'Fill element', 'tif-slider' ),
						// 'contain'			=> esc_html__( 'Maintain ratio', 'tif-slider' ),
					),
					'input_attrs'		=> array(
						'min'				=> '0',
						'step'				=> '1',
						'unit'				=> array(
							'px'				=> esc_html__( 'px', 'tif-slider' ),
							// 'rem'				=> esc_html__( 'rem', 'tif-slider' ),
							'vh'				=> esc_html__( 'vh', 'tif-slider' ),
							'%'					=> esc_html__( '%', 'tif-slider' ),
						),
						'alignment'			=> 'row',
					),
					'settings'		=> $tif_plugin_name . '[tif_layout][cover_fit]',
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_slider_panel_layout_section_features_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_layout_section_features_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_layout_section',
					'label'         => esc_html__( 'Layout', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][features]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_layout,features', 'multicheck' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Checkbox_Multiple_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_layout][features]',
				array(
					'section' => 'tif_plugin_slider_panel_layout_section',
					'label'   => esc_html__( 'Slider features', 'tif-slider' ),
					'choices' => array(
						'arrows'              => esc_html__( 'Arrows', 'tif-slider' ),
						'bullets'             => esc_html__( 'Bullets', 'tif-slider' ),
					)
				)
			)
		);

		if ( ! class_exists( 'Themes_In_France' ) ) {

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_layout][boxed_width]',
				array(
					'default'           => tif_get_default( 'plugin_slider', 'tif_layout,boxed_width', 'length' ),
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_length'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Number_Multiple_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_layout][boxed_width]',
					array(
						'section'           => 'tif_plugin_slider_panel_layout_section',
						'priority'          => 10,
						'label'             => esc_html__( 'Boxed width', 'tif-slider' ),
						'choices'           => array(
							'value'             => esc_html__( 'Value', 'tif-slider' ),
							'unit'              => esc_html__( 'Unit', 'tif-slider' ),
						),
						'input_attrs'       => array(
							'min'               => '800',
							// 'max'               => '50',
							'step'              => '1',
							'unit'              => array(
								'px'                => esc_html__( 'px', 'tif-slider' ),
							),
							'alignment'         => 'column',
						),
					)
				)
			);

		}

		$wp_customize->add_setting(
			'tif_theme_post_cover[tif_layout][alignment][heading]',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_theme_post_cover[tif_layout][alignment][heading]',
				array(
					'section'       => 'tif_plugin_slider_panel_layout_section',
					'label'         => esc_html__( 'Box Alignment', 'tif-slider' ),
				)
			)
		);

		$tif_slider_alignment = tif_get_default( 'plugin_slider', 'tif_layout,alignment', 'array' );


		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][alignment][align_items]',
			array(
				'default'           => tif_sanitize_key( $tif_slider_alignment['align_items'] ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_select',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][alignment][align_items]',
			array(
				'section'           => 'tif_plugin_slider_panel_layout_section',
				'label'             => esc_html__( 'Horizontal align', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => array(
					'start'             => esc_html__( 'Start', 'tif-slider' ),
					'center'            => esc_html__( 'Center', 'tif-slider' ),
					'end'               => esc_html__( 'End', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][alignment][justify_content]',
			array(
				'default'           => tif_sanitize_key( $tif_slider_alignment['justify_content'] ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_select',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][alignment][justify_content]',
			array(
				'section'           => 'tif_plugin_slider_panel_layout_section',
				'label'             => esc_html__( 'Vertical align', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => tif_get_justify_content_options()
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_layout][alignment][gap]',
			array(
				'default'           => tif_sanitize_key( $tif_slider_alignment['gap'] ),
				'type'              => 'option',
				'sanitize_callback' => 'tif_sanitize_select',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_layout][alignment][gap]',
			array(
				'section'           => 'tif_plugin_slider_panel_layout_section',
				'label'             => esc_html__( 'Vertical spacing', 'tif-slider' ),
				'description'       => esc_html__( 'Defines the vertical space between the elements.', 'tif-slider' ),
				'type'              => 'select',
				'choices'           => tif_get_gap_array( 'label' )
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_slider_panel_layout_section_buttons_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_layout_section_buttons_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_layout_section',
					'label'         => esc_html__( 'Buttons', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		for ( $i = 1; $i <= 2; $i++ ) {

			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_layout][btn' . $i . '_class]',
				array(
					'default'           => tif_get_default( 'plugin_slider', 'tif_layout,btn' . $i . '_class', 'key' ),
					'type'              => 'option',
					'sanitize_callback' => 'tif_sanitize_string',
				)
			);
			$wp_customize->add_control(
				$tif_plugin_name . '[tif_layout][btn' . $i . '_class]',
				array(
					'section'           => 'tif_plugin_slider_panel_layout_section',
					'label'             => esc_html__( 'Button style', 'tif-slider' ) . ' #' . $i,
					'type'              => 'select',
					'choices'           => array(
						'btn--default'        => esc_html__( 'Default button', 'tif-slider' ),
						'btn--primary'        => esc_html__( 'Primary button', 'tif-slider' ),
						'btn--primary alt'    => esc_html__( 'Alt Primary button', 'tif-slider' ),
						'btn--success'        => esc_html__( 'Success button', 'tif-slider' ),
						'btn--info'           => esc_html__( 'Info button', 'tif-slider' ),
						'btn--warning'        => esc_html__( 'Warning button', 'tif-slider' ),
						'btn--danger'         => esc_html__( 'Danger button', 'tif-slider' ),
					)
				)
			);
		}

		// ... SECTION // Slider Colors ........................................

		$wp_customize->add_section(
			'tif_plugin_slider_panel_colors_section',
			array(
				'priority'          => 601,
				'title'             => esc_html__( 'Colors', 'tif-slider' ),
				'panel'             => 'tif_plugin_slider_panel'
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_slider_panel_colors_section_background_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_colors_section_background_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_colors_section',
					'label'         => esc_html__( 'Background colors', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][bgcolor]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,bgcolor', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][bgcolor]',
				array(
					'section'           => 'tif_plugin_slider_panel_colors_section',
					'label'             => esc_html__( 'Background', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'            => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][overlay_bgcolor]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,overlay_bgcolor', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][overlay_bgcolor]',
				array(
					'section'           => 'tif_plugin_slider_panel_colors_section',
					'label'             => esc_html__( 'Overlay', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => true,
					),
				)
			)
		);


		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][title_bgcolor]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,title_bgcolor', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][title_bgcolor]',
				array(
					'section'           => 'tif_plugin_slider_panel_colors_section',
					'label'             => esc_html__( 'Title', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => true,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][subtitle_bgcolor]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,subtitle_bgcolor', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][subtitle_bgcolor]',
				array(
					'section'           => 'tif_plugin_slider_panel_colors_section',
					'label'             => esc_html__( 'Sub title', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => true,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][excerpt_bgcolor]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,excerpt_bgcolor', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][excerpt_bgcolor]',
				array(
					'section'           => 'tif_plugin_slider_panel_colors_section',
					'label'             => esc_html__( 'Excerpt', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_slider_panel_colors_section_text_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_slider_panel_colors_section_text_heading',
				array(
					'section'       => 'tif_plugin_slider_panel_colors_section',
					'label'         => esc_html__( 'Text colors', 'tif-slider' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][title_color]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,title_color', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][title_color]',
				array(
					'section'       => 'tif_plugin_slider_panel_colors_section',
					'label'         => esc_html__( 'Title', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][subtitle_color]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,subtitle_color', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][subtitle_color]',
				array(
					'section'       => 'tif_plugin_slider_panel_colors_section',
					'label'         => esc_html__( 'Sub title', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_colors][excerpt_color]',
			array(
				'default'           => tif_get_default( 'plugin_slider', 'tif_colors,excerpt_color', 'array_keycolor' ),
				'type'              => 'option',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_colors][excerpt_color]',
				array(
					'section'       => 'tif_plugin_slider_panel_colors_section',
					'label'         => esc_html__( 'Excerpt', 'tif-slider' ),
					'choices'           => tif_get_theme_hex_colors(),
					'input_attrs'       => array(
						'format'            => 'hex',	// key, hex
						// 'tif'               => 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'            => 'array',
						'brightness'        => false,
						'opacity'           => false,
					),
				)
			)
		);

	// ... SECTION // Slider Content #1 to #5 ..................................

		for ( $i = 1; $i <= 5; $i++ ) {

			$slider[$i] = tif_get_default( 'plugin_slider', 'tif_slider_content,slider_' . $i, 'array' );

			$wp_customize->add_section(
				'tif_plugin_slider' . $i . '_options_section',
				array(
					'priority'          => 604,
					'title'             => sprintf(
						'%1$s #%2$s',
						esc_html__( 'Slider', 'tif-slider' ),
						(int)$i
					),
					'panel'             => 'tif_plugin_slider_panel'
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][thumbnail_id]',
				array(
					// 'default'           => TIF_SLIDER_IMG_URL . 'slide' . $i . '.jpg',
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'absint'
				)
			);
			$wp_customize->add_control(
				// new WP_Customize_Image_Control(	// url
				new WP_Customize_Media_Control(		// media id
					$wp_customize,
					$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][thumbnail_id]',
					array(
						'section'       => 'tif_plugin_slider' . $i . '_options_section',
						'label'         => esc_html__( 'Upload image', 'tif-slider' ),
						'setting'       => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][thumbnail_id]'
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title_link]',
				array(
					'default'           => esc_url( $slider[$i]['title_link'] ),
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_url'
				)
			);
			$wp_customize->add_control(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title_link]',
				array(
					'section'           => 'tif_plugin_slider' . $i . '_options_section',
					'label'             => esc_html__( 'Link for this slide', 'tif-slider' ),
					// 'description'       => esc_html__( 'Leave blank to disable', 'tif-slider' ),
					'setting'           => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title_link]',
					'type'              => 'url',
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_slider' . $i . '_options_section_title_heading',
				array(
					'sanitize_callback' => 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_slider' . $i . '_options_section_title_heading',
					array(
						'section'       => 'tif_plugin_slider' . $i . '_options_section',
						'label'         => esc_html__( 'Title', 'tif-slider' ),
					)
				)
			);

			// Add Setting
			// ...
			$tif_slider_title[$i] = tif_sanitize_wrap_attr( $slider[$i]['title'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title]',
				array(
					'default'           => (array)$tif_slider_title[$i],
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title]',
					array(
						'section'           => 'tif_plugin_slider' . $i . '_options_section',
						// 'label'                => esc_html__( 'Title', 'tif-slider' ),
						'choices'           => array(
							0	=> array(
								'id'            => 'content',
								'label'         => false,
								'type'          => 'text',
							),
							1	=> array(
								'id'            => 'container_tag',
								'description'   => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-slider' ),
									esc_html( isset( $tif_slider_title[$i][1] ) ? $tif_slider_title[$i][1] : null ),
								)
							),
							2	=> array(
								'id'            => 'container_class',
							),
						),
						'settings'          => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][title]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_slider' . $i . '_options_section_sub_title_heading',
				array(
					'sanitize_callback' => 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_slider' . $i . '_options_section_sub_title_heading',
					array(
						'section'       => 'tif_plugin_slider' . $i . '_options_section',
						'label'         => esc_html__( 'Sub title', 'tif-slider' ),
					)
				)
			);

			$tif_slider_subtitle[$i] = tif_sanitize_wrap_attr( $slider[$i]['subtitle'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][subtitle]',
				array(
					'default'           => (array)$tif_slider_subtitle[$i],
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][subtitle]',
					array(
						'section'             => 'tif_plugin_slider' . $i . '_options_section',
						// 'label'               => esc_html__( 'Sub title', 'tif-slider' ),
						'choices'             => array(
							0	=> array(
								'id'            => 'content',
								'type'          => 'text',
								'label'         => false,
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-slider' ),
								// 	esc_html( isset( $tif_slider_subtitle[$i][0] ) ? $tif_slider_subtitle[$i][0] : null ),
								// )
							),
							1	=> array(
								'id'            => 'container_tag',
								'description'   => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-slider' ),
									esc_html( isset( $tif_slider_subtitle[$i][1] ) ? $tif_slider_subtitle[$i][1] : null ),
								)
							),
							2	=> array(
								'id'            => 'container_class',
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-slider' ),
								// 	esc_html( isset( $tif_slider_subtitle[$i][2] ) ? $tif_slider_subtitle[$i][2] : null ),
								// )
							),
						),
						'settings'              => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][subtitle]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_slider' . $i . '_options_section_excerpt_heading',
				array(
					'sanitize_callback' => 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_slider' . $i . '_options_section_excerpt_heading',
					array(
						'section'       => 'tif_plugin_slider' . $i . '_options_section',
						'label'         => esc_html__( 'Description', 'tif-slider' ),
					)
				)
			);

			// Add Setting
			// ...
			$tif_slider_excerpt[$i] = tif_sanitize_wrap_attr( $slider[$i]['excerpt'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][excerpt]',
				array(
					'default'           => (array)$tif_slider_excerpt[$i],
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][excerpt]',
					array(
						'section'           => 'tif_plugin_slider' . $i . '_options_section',
						// 'label'             => esc_html__( 'Description for this slide', 'tif-slider' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'textarea',
								'label'       => false,
								// 'description' => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-slider' ),
								// 	esc_html( isset( $tif_slider_excerpt[$i][0] ) ? $tif_slider_excerpt[$i][0] : null ),
								// )
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-slider' ),
									esc_html( isset( $tif_slider_excerpt[$i][1] ) ? $tif_slider_excerpt[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								// 'description'       => sprintf( '%s "%s"',
								// 	esc_html__( 'Default value:', 'tif-slider' ),
								// 	esc_html( isset( $tif_slider_excerpt[$i][2] ) ? $tif_slider_excerpt[$i][2] : null ),
								// )
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][excerpt]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_slider' . $i . '_options_section_buttons_heading',
				array(
					'sanitize_callback' => 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_slider' . $i . '_options_section_buttons_heading',
					array(
						'section'       => 'tif_plugin_slider' . $i . '_options_section',
						'label'         => esc_html__( 'Buttons', 'tif-slider' ),
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_txt]',
				array(
					'default'           => esc_html( $slider[$i]['btn1_txt'] ),
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_string'
				)
			);

			$wp_customize->add_control(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_txt]',
				array(
					'section'           => 'tif_plugin_slider' . $i . '_options_section',
					'label'             => esc_html__( 'Text of the first button', 'tif-slider' ),
					'description'       => esc_html__( 'Leave blank to disable', 'tif-slider' ),
					'setting'           => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_txt]'
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_link]',
				array(
					'default'           => esc_url( $slider[$i]['btn1_link'] ),
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_url'
				)
			);

			$wp_customize->add_control(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_link]',
				array(
					'section'           => 'tif_plugin_slider' . $i . '_options_section',
					'label'             => esc_html__( 'Link of the first button', 'tif-slider' ),
					'description'       => esc_html__( 'Both used for the slide and the first button', 'tif-slider' ),
					'setting'           => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn1_link]',
					'type'              => 'url'
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_txt]',
				array(
					'default'           => '',
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'tif_sanitize_string'
				)
			);

			$wp_customize->add_control(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_txt]',
				array(
					'section'           => 'tif_plugin_slider' . $i . '_options_section',
					'label'             => esc_html__( 'Text of the second button', 'tif-slider' ),
					'description'       => esc_html__( 'Leave blank to disable', 'tif-slider' ),
					'setting'           => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_txt]'

				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_link]',
				array(
					'default'           => '',
					'type'              => 'option',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'sanitize_url'
				)
			);

			$wp_customize->add_control(
				$tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_link]',
				array(
					'section'           => 'tif_plugin_slider' . $i . '_options_section',
					'label'             => esc_html__( 'Link of the second button', 'tif-slider' ),
					'setting'           => $tif_plugin_name . '[tif_slider_content][slider_' . $i . '][btn2_link]',
					'type'              => 'url'
				)
			);

		}

	}

}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'Tif_Slider_Customize' , 'register' ) );
