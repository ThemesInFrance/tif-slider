<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_color_control' ) ) {

	add_action( 'customize_register', 'tif_color_control' );

	function tif_color_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Color_Control extends WP_Customize_Control {

			public $type = 'tif-colors';
			/**
			 * Render Settings
			 */
			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				/* Data */
				$values = tif_sanitize_array_keycolor( $this->value() );
				$format = isset ( $this->input_attrs['format'] ) ? tif_sanitize_key( $this->input_attrs['format'] ) : 'hex' ;
				$output = isset ( $this->input_attrs['output'] ) ? tif_sanitize_key( $this->input_attrs['output'] ) : null ;
				// $name   = '_customize-' . esc_attr( $format ) . '-color-' . $this->id;

				if ( isset ( $this->input_attrs['tif'] ) && $this->input_attrs['tif'] == 'key' && class_exists ( 'Themes_In_France' ) ) {
					$format = 'key';
					$output = 'array';
					$this->choices = tif_get_theme_colors_array();
				}

				$alphaElement = $input_opacity = null;

				echo '<ul class="tif-' . esc_attr( $format ) . '-' . esc_attr( $output ) . '-color">';

				if ( $format == 'key' ) {

					if ( empty( $this->choices ) )
						return;

					switch ( $values[1] ) {
						case 'lightest':
							$range_value = '-60';
							break;
							
						case 'lighter':
							$range_value = '-40';
							break;
							
						case 'light':
							$range_value = '-20';
							break;
							
						case 'dark':
							$range_value = '20';
							break;
							
						case 'darker':
							$range_value = '40';
							break;
							
						case 'darkest':
							$range_value = '60';
							break;
							
						default:
							$range_value = '0';
							break;
							
					}

					foreach ( $this->choices as $value => $label ) :

						?>

						<li class="tif-key-array-color-item">

							<input type="radio"
								id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
								value="<?php echo esc_attr( $value ); ?>"
								name="<?php echo esc_attr( $name ); ?>"
								class="tif-key-input-colors"
								<?php
								$this->link();
								checked( $value,(string)$values[0] );
								?>
							/>

							<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>" class="tif-color-<?php echo tif_sanitize_slug( $value ) ?>">
								<img
									src="<?php echo TIF_THEME_ADMIN_IMAGES_URL . '/blank.png' ?>"
									style="background:<?php echo $label['value'] ?>;"
									class="tif-<?php echo tif_esc_css( $value ) ?>-background-color"
									title="<?php echo esc_html( $label['label'] ) ?>"
								/>
							</label>

						</li>

						<?php

					endforeach;

					if ( isset ( $this->input_attrs['brightness'] ) && $this->input_attrs['brightness'] ) {

					?>

						<li class="clearfix">

							<ul class="tif-brightness-range">

								<li>

									<label>
										<span class="lighter"><?php echo esc_html__( 'Lighter', 'canopee' ) ?></span>
										<span class="normal"><?php echo esc_html__( 'Normal', 'canopee' ) ?></span>
										<span class="darker"><?php echo esc_html__( 'Darker', 'canopee' ) ?></span>
										<input type="range"
											class="tif-brightness-input-range"
											value="<?php echo (int)$range_value; ?>"
											min="-40"
											max="40"
											step="20"
										/>
									</label>

								</li>

							</ul>

						</li>

					<?php

					}

					if ( isset ( $this->input_attrs['opacity'] ) && $this->input_attrs['opacity'] ) {

					?>

					<li class="clearfix">

						<ul class="tif-multirange row">

							<li>

								<label>
									<span><?php echo esc_html__( 'Opacity', 'canopee' ) ?></span>
									<input type="range"
										class="tif-opacity-input-range"
										value="<?php echo (float)$values[2]; ?>"
										min="<?php echo (float)(isset ( $this->input_attrs['opacity']['min'] ) ? $this->input_attrs['opacity']['min'] : 0) ?>"
										max="<?php echo (float)isset ( $this->input_attrs['opacity']['max'] ) ? $this->input_attrs['opacity']['max'] : 1 ?>"
										step="<?php echo (float)isset ( $this->input_attrs['opacity']['step'] ) ? $this->input_attrs['opacity']['step'] : .05 ?>"
									/>
								</label>

							</ul>

						</li>

					</li>

					<?php

					}

				} else {

				?>

					<li>

						<?php

						if ( isset ( $this->input_attrs['opacity'] ) && $this->input_attrs['opacity'] && $output == 'array' ) {
							$alphaElement = "alphaElement:'#" . tif_sanitize_slug( $name ) . "',";
							$input_opacity =  '<input type="number"
								id="' . tif_sanitize_slug( $name ) . '"
								class="tif-jscolor-input-number"
								value="' . (float)$values[2] . '"
								min="0"
								max="1"
								step="0.01" />';
							}

						?>

						<input type="text"
						<?php if ( $output != 'array' ) $this->link() ?>
							class="tif-jscolor-input-text"
							value="<?php echo (string)$values[0] ?>"
							data-jscolor="{
								format: '<?php echo esc_attr( $format ); ?>',
								value: '<?php echo (string)$values[0] ?>',
								<?php echo esc_attr( $alphaElement ); ?>
								palette: '<?php echo implode( ',', $this->choices ) ?>'
							}"
						/>

						<?php

						echo $input_opacity;

						?>

					</li>

					<script>
						jscolor.install();
					</script>

				<?php

				}

				if ( $output == 'array' || $format == 'key') {
					?>
					<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />
					<?php
				}

				?>

					</ul><!-- .tif-<?php echo esc_attr( $format ) ?>-color -->

				<?php

			}

			/**
			 * Enqueue our scripts and styles
			 */
			public function enqueue() {

				if ( $this->input_attrs['format'] != 'key' ) {

					wp_enqueue_style( 'tif-jscolor', Tif_Init::tif_get_tif_url() . 'assets/css/tif-jscolor.min.css', false, '1.0', false );
					wp_enqueue_script( 'tif-jscolor', Tif_Init::tif_get_tif_url() . 'assets/js/tif-jscolor.min.js', false, '1.0', false );

				}

			}

		}

	}

}
